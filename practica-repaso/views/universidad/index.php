<div class="container" style="margin-top: 80px">
    <div class="jumbotron">
        <h2>Listado de Universidades</h2>
    </div>
    <div class="row">
        <?php foreach($queryUni as $dataUni): ?>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?= $dataUni['nombre_universidad'] ?></h5>
                        <p class="card-text"><strong>Carreras:</strong> <?= $dataUni['carreras'] ?></p>
                        
                        <?php 
                        $carreras = explode(',', $dataUni['carreras']);
                        $descripciones = explode(',', $dataUni['descripciones']);
                        for ($i = 0; $i < count($carreras); $i++): ?>
                            <p class="card-text"><strong><?= $carreras[$i] ?>:</strong> <?= $descripciones[$i] ?></p>
                        <?php endfor; ?>
                        
                        <?php if(isset($dataUni['direccion'])): ?>
                            <p class="card-text"><strong>Dirección:</strong> <?= $dataUni['direccion'] ?></p>
                        <?php endif; ?>

                        <?php if(isset($dataUni['telefono'])): ?>
                            <p class="card-text"><strong>Teléfono:</strong> <?= $dataUni['telefono'] ?></p>
                        <?php endif; ?>
                        <a href="index.php?metodo=universidad&id_universidad=<?= $dataUni['id_universidad']?>" class="btn btn-primary">Editar</a>
                        <a href="index.php?metodo=confirmarEliminacionUniversidad&id_universidad=<?= $dataUni['id_universidad']?>" class="btn btn-danger">Eliminar</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
