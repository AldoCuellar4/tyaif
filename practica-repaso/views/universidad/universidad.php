<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alta Universidad</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .container {
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .jumbotron {
            background-color: #007bff;
            color: white;
            text-align: center;
            padding: 20px;
            margin-bottom: 30px;
            width: 100%;
            max-width: 400px;
            border-radius: 5px;
        }
        form {
            background-color: white;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            width: 100%;
            max-width: 400px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h1>Agregar Universidad</h1>
        </div>
        <form class="form-horizontal" action="<?php echo isset($dataUni['id_universidad']) && $dataUni['id_universidad'] != "" ? 'index.php?metodo=obtenerDatosEdicionUniversidad&id_universidad=' . $dataUni['id_universidad'] : 'index.php?metodo=obtenerDatosEdicionUniversidad'; ?>" method="post">
            <?php if(isset($dataUni['id_universidad']) && $dataUni['id_universidad'] == ""): ?>
                <input type="hidden" class="form-control" name="id_universidad" value="<?= $dataUni['id_universidad'] ?>">
            <?php endif; ?>
            <div class="form-group">
                <label for="nombre_universidad">Nombre de la Universidad:</label>
                <input type="text" class="form-control" name="nombre_universidad" value="<?= isset($dataUni['nombre_universidad']) ? $dataUni['nombre_universidad'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="direccion">Dirección de la Universidad:</label>
                <input type="text" class="form-control" name="direccion" value="<?= isset($dataUni['direccion']) ? $dataUni['direccion'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="telefono">Teléfono de la Universidad:</label>
                <input type="text" class="form-control" name="telefono" value="<?= isset($dataUni['telefono']) ? $dataUni['telefono'] : '' ?>">
            </div>
            <div class="form-group">
                <label>Carreras:</label>
                <div class="list-group">
                    <?php foreach ($carreras as $carrera): ?>
                        <label class="list-group-item">
                            <input type="checkbox" name="carreras[]" value="<?= $carrera['id_carrera'] ?>" <?= (isset($carreras_seleccionadas) && in_array($carrera['id_carrera'], $carreras_seleccionadas)) ? 'checked' : '' ?>>
                            <?= $carrera['nombre_carrera'] ?>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" value="<?= isset($dataUni['id_universidad']) && $dataUni['id_universidad'] == "" ? 'Agregar' : 'Actualizar' ?>">
            </div>
        </form>
    </div>
</body>
</html>
