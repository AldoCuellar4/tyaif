<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .container {
            min-height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .jumbotron {
            background-color: #007bff;
            color: white;
            text-align: center;
            padding: 20px;
            margin-bottom: 30px;
            width: 100%;
            max-width: 400px;
            border-radius: 5px;
        }
        form {
            background-color: white;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            width: 100%;
            max-width: 400px;
        }
        .form-container {
            margin: 20px;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="form-container">
            <div class="jumbotron">
                <h1>Agregar Carrera</h1>
            </div>
            <form class="form-horizontal" action="<?php echo isset($data['id_carrera']) && $data['id_carrera'] != "" ? 'index.php?metodo=obtenerDatosEdicion&id_carrera=' . $data['id_carrera'] : 'index.php?metodo=obtenerDatosEdicion'; ?>" method="post">
                <?php if(isset($data['id_carrera']) && $data['id_carrera'] == ""): ?>
                    <input type="hidden" class="form-control" name="id_carrera" value="<?= $data['id_carrera'] ?>">
                <?php endif; ?>
                <div class="form-group">
                    <label for="nombre_carrera">Nombre de la Carrera:</label>
                    <input type="text" class="form-control" name="nombre_carrera" value="<?= isset($data['nombre_carrera']) ? $data['nombre_carrera'] : '' ?>">
                </div>
                <div class="form-group">
                    <label for="descripcion_carrera">Descripción de la Carrera:</label>
                    <textarea class="form-control" name="descripcion_carrera" rows="3"><?= isset($data['descripcion_carrera']) ? $data['descripcion_carrera'] : '' ?></textarea>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input type="submit" class="btn btn-primary form-control" value="<?= isset($data['id_carrera']) && $data['id_carrera'] == "" ? 'Registrar' : 'Actualizar' ?>">
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
