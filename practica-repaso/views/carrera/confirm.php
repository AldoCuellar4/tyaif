<section class="container">
    <div class="row">
        <form method="post" action="index.php?metodo=eliminarCarrera">
            <div class="col-md-6 offset-md-3">
                <label>¿Deseas eliminar este registro?</label>
                <input type="hidden" name="id_carrera" value="<?php echo $_REQUEST['id_carrera']; ?>">
                <input type="hidden" name="confirmacion" value="confirmado">
                <input type="submit" value="Eliminar" class="form-control btn btn-danger">
            </div>
        </form>
    </div>
</section>
