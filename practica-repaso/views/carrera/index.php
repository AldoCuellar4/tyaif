<div class="container" style="margin-top: 80px">
    <div class="jumbotron">
        <h2>Listado de carreras</h2>
    </div>
    <div class="container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Carrera</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($query as $data): ?>
                    <tr>
                        <td><?= $data['nombre_carrera'] ?></td>
                        <td><?= $data['descripcion_carrera'] ?></td>
                        <td>
                            <a href="index.php?metodo=carrera&id_carrera=<?= $data['id_carrera']?>" class="btn btn-primary">Editar</a>
                            <a href="index.php?metodo=eliminarCarrera&id_carrera=<?= $data['id_carrera']?>" class="btn btn-danger">Eliminar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
