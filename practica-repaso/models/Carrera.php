<?php
    
class CarreraModelo{
    private $DB;

    function __construct(){
        $this->DB=Database::connect();
    }

    function obtener(){
        $sql= 'SELECT * FROM carreras ORDER BY id_carrera DESC';
        $stmt=$this->DB->query($sql);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function crear($data){
        $carreraExistente = $this->obtenerPorNombre($data['nombre_carrera']);
        if (!$carreraExistente) {
            $sql = "INSERT INTO carreras(nombre_carrera, descripcion_carrera) VALUES (?, ?)";
            $this->DB->prepare($sql)->execute([$data['nombre_carrera'], $data['descripcion_carrera']]);
            header("Location:index.php");
        } else {
            echo "<script>alert('La carrera ya existe'); window.location.href = 'index.php?metodo=carrera';</script>";
        }
    }
    
    function obtenerPorNombre($nombre_carrera){
        $sql = "SELECT * FROM carreras WHERE nombre_carrera = ?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$nombre_carrera]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    function obtenerPorId($id){
        $sql = "SELECT * FROM carreras WHERE id_carrera = ?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function actualizar($data, $id){
        $sql = "UPDATE carreras SET nombre_carrera = ?, descripcion_carrera = ? WHERE id_carrera = ?";
        $this->DB->prepare($sql)->execute([$data['nombre_carrera'], $data['descripcion_carrera'], $id]);
        header("Location:index.php");
    }

    function eliminar($id){
        $sql_check = "SELECT COUNT(*) as count FROM universidades_carreras WHERE id_carrera = ?";
        $stmt = $this->DB->prepare($sql_check);
        $stmt->execute([$id]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result['count'] > 0) {
            echo "<script>
                    alert('La carrera no se puede eliminar porque está asociada a una o más universidades.'); 
                    window.location.href = 'index.php?';
                  </script>";
        } else {
            $sql = "DELETE FROM carreras WHERE id_carrera=?";
            $this->DB->prepare($sql)->execute([$id]);
            header("Location: index.php");
        }
    }
}
?>
