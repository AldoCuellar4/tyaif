<?php
    
    class RegistrarModelo{
        private $DB;

        function __construct(){
            $this->DB=Database::connect();
        }

         //Función para poder insertar un usuario en la tabla llamada usuario
        function insertarUsuario($nombre, $correo, $contraseña){
            $this->DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql="INSERT INTO usuarios (nombre,correo,contraseña,rol) VALUES (?,?,?,?)";
            $query = $this->DB->prepare($sql);
            $query->execute(array($nombre,$correo,$contraseña,'usuario'));
            header("Location:index.php?metodo=login");
            Database::disconnect();       
        }
    
    }
?>

