<?php
    
    class IniciarModelo{
        private $DB;

        function __construct(){
            $this->DB=Database::connect();
        }

        public function validarUsuario($usuario, $contraseña) {

            $this->DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //consulta para validar al usuario    
            $sql = "SELECT * FROM usuarios WHERE nombre = ? AND contraseña = ?";
            $q = $this->DB->prepare($sql);
            $q->execute(array($usuario,$contraseña));
            // Verificar si se encontró un usuario con los datos proporcionados
            if ($q->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
    
    }
?>

