<?php
    
class UniversidadModelo{
    private $DB;
    private $universidad;

    function __construct(){
        $this->DB = Database::connect();
    }

    function obtenerUniversidades(){
        //consulta para mostrar las universidades con la descripción de la carrera
        $sqlUni = 'SELECT universidades.*, IFNULL(GROUP_CONCAT(carreras.nombre_carrera), "Ninguna") AS carreras,
           IFNULL(GROUP_CONCAT(carreras.descripcion_carrera), "Ninguna") AS descripciones
           FROM universidades
           LEFT JOIN universidades_carreras ON universidades.id_universidad = universidades_carreras.id_universidad
           LEFT JOIN carreras ON universidades_carreras.id_carrera = carreras.id_carrera
           GROUP BY universidades.id_universidad
           ORDER BY universidades.id_universidad DESC';
        
        $consulta = $this->DB->query($sqlUni);
        $this->universidad = $consulta->fetchAll(PDO::FETCH_ASSOC);
    
        return $this->universidad;
    }
    

    function obtenerPorNombre($nombre_universidad){
        $sql = "SELECT * FROM universidades WHERE nombre_universidad = ?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$nombre_universidad]);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function obtenerPorId($id_universidad){
        $sql = "SELECT * FROM universidades WHERE id_universidad = ?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$id_universidad]);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function crearUniversidad($dataUni){
        $existeUniversidad = $this->obtenerPorNombre($dataUni['nombre_universidad']);

        if (!$existeUniversidad) {
            $sqlUni = "INSERT INTO universidades (nombre_universidad, direccion, telefono) VALUES (?, ?, ?)";
            $stmt = $this->DB->prepare($sqlUni);
            $stmt->execute([$dataUni['nombre_universidad'], $dataUni['direccion'], $dataUni['telefono']]);

            $lastInsertedId = $this->DB->lastInsertId();
            header("Location: index.php?metodo=indexPrincipal");

            return $lastInsertedId;
        } else {
            echo "<script>alert('La universidad ya existe'); window.location.href = 'index.php?metodo=universidad';</script>";
        }
    }

    function actualizarUniversidad($dataUni, $idUniversidad, $carrerasSeleccionadas){
        $sql = "UPDATE universidades SET nombre_universidad=?, ubicacion=? WHERE id_universidad=?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$dataUni['nombre_universidad'], $dataUni['ubicacion'], $idUniversidad]);

        $this->eliminarCarrerasUniversidad($idUniversidad);

        foreach ($carrerasSeleccionadas as $carreraId) {
            $this->insertarCarreraUniversidad($idUniversidad, $carreraId);
        }

        header("Location:index.php?metodo=indexPrincipal");
    }

    function eliminarCarrerasUniversidad($id_universidad){
        $sql = "DELETE FROM universidades_carreras WHERE id_universidad=?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$id_universidad]);
    }

    function eliminarUniversidad($id_universidad){
        $sql = "DELETE FROM universidades WHERE id_universidad=?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$id_universidad]);

        header("Location:index.php?metodo=indexPrincipal");
    }

    function insertarCarreraUniversidad($idUniversidad, $idCarrera) {
        $sql = "INSERT INTO universidades_carreras (id_universidad, id_carrera) VALUES (?, ?)";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$idUniversidad, $idCarrera]);
    }

    function obtenerCarrerasUniversidad($id_universidad) {
        $sql = "SELECT id_carrera FROM universidades_carreras WHERE id_universidad = ?";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute([$id_universidad]);

        $carreras = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $carreras[] = $row['id_carrera'];
        }

        return $carreras;
    }

}
?>
