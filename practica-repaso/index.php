<?php
session_start();

// verificar si la sesión no está iniciada
if (!isset($_SESSION['esUsuario']) || !$_SESSION['esUsuario']) {
    // verificar si el metodo no es de inicio de sesión ni de registro
    if ($_REQUEST['metodo'] != 'login' && $_REQUEST['metodo'] != 'registro') {
        // redirigir a la página de inicio de sesión
        header("Location: index.php?metodo=login");
    }
}

require_once('bd/Conexion.php');
//importar controllers
require_once('controllers/CarreraController.php');
require_once('controllers/UniversidadController.php');
require_once('controllers/IniciarController.php'); 
require_once('controllers/RegistrarController.php'); 

// Crear instancias de los controladores
$carreraController = new CarreraController();
$universidadController = new UniversidadController();
$iniciarController = new IniciarController(); 
$registroController = new RegistrarController();

if (!empty($_REQUEST['metodo'])) {
    $metodo = $_REQUEST['metodo'];
    
    // Verificar si el método existe en el controlador de Carrera
    if (method_exists($carreraController, $metodo)) {
        $carreraController->$metodo(); // Llamar al método del controlador de Carrera
    }
    // Verificar si el método existe en el controlador de Universidad
    elseif (method_exists($universidadController, $metodo)) {
        $universidadController->$metodo(); // Llamar al método indexUniversidades del controlador de Universidad
    } 
    elseif (method_exists($registroController, $metodo)) {
        $registroController->$metodo(); // Llamar al método del controlador de registro
    }
    elseif ($metodo === 'indexPrincipal') {
        $universidadController->index();
    }
    elseif (method_exists($iniciarController, $metodo)) {
            $iniciarController->$metodo(); // Llamar al método indexUniversidades del controlador de Universidad
    } 
    else {
        $carreraController->index(); // Si no existe el método, cargar el índice del controlador de Carrera
    }
} else {
    $carreraController->index(); // Si no se proporciona ningún método, cargar el índice del controlador de Carrera
}
?>
