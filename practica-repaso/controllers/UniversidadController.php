<?php 
require_once('models/Universidad.php');

class UniversidadController{

    private $universidadModel;

    function __construct(){
        $this->universidadModel = new UniversidadModelo();
    }

    function index(){
        $queryUni = $this->universidadModel->obtenerUniversidades();
        include_once('views/index/header.php');
        include_once('views/universidad/index.php');
    }

    function universidad(){
        $modeloCarrera = new CarreraModelo();
        $carreras = $modeloCarrera->obtener();
        $datosUniversidad = array('id_universidad' => '');

        if(isset($_REQUEST['id_universidad'])){
            $datosUniversidad = $this->universidadModel->obtenerPorId($_REQUEST['id_universidad']);    
            $carrerasSeleccionadas = $this->universidadModel->obtenerCarrerasUniversidad($_REQUEST['id_universidad']);
        }
        $consultaUniversidades = $this->universidadModel->obtenerUniversidades();
        include_once('views/index/header.php');
        include_once('views/universidad/universidad.php');
    }

    function obtenerDatosEdicionUniversidad(){ 
        
        if(isset($_REQUEST['id_universidad'])) {
            $datosUniversidad['id_universidad'] = $_REQUEST['id_universidad'];
        } else {
            // Manejar el caso en el que $_REQUEST['id_universidad'] no está definida
        }
        $datosUniversidad['nombre_universidad'] = isset($_REQUEST['nombre_universidad']) ? $_REQUEST['nombre_universidad'] : '';
        // Verificar la existencia de la clave 'ubicacion'
        $datosUniversidad['ubicacion'] = isset($_REQUEST['ubicacion']) ? $_REQUEST['ubicacion'] : '';
        $datosUniversidad['direccion'] = isset($_REQUEST['direccion']) ? $_REQUEST['direccion'] : '';
        $datosUniversidad['telefono'] = isset($_REQUEST['telefono']) ? $_REQUEST['telefono'] : '';
        $carrerasSeleccionadas = isset($_REQUEST['carreras']) ? $_REQUEST['carreras'] : array();
        
        if ($_REQUEST['id_universidad'] == "") {
            if (empty($carrerasSeleccionadas)) {
                echo "<script>
                        alert('Seleccione una carrera'); 
                        window.location.href = 'index.php?metodo=universidad';
                    </script>";
            } else {
                $ultimoIdInsertado = $this->universidadModel->crearUniversidad($datosUniversidad);
                foreach ($carrerasSeleccionadas as $idCarrera) {
                    $this->universidadModel->insertarCarreraUniversidad($ultimoIdInsertado, $idCarrera);
                }
            }
        } else {
            $idUniversidad = $_REQUEST['id_universidad'];
            if (empty($carrerasSeleccionadas)) {
                $this->universidadModel->eliminarCarrerasUniversidad($idUniversidad);
            }
            $this->universidadModel->actualizarUniversidad($datosUniversidad, $idUniversidad, $carrerasSeleccionadas);
        }
    }

    function confirmarEliminacionUniversidad() {
        if(isset($_REQUEST['id_universidad'])) {
            $id_universidad = $_REQUEST['id_universidad'];
            include_once('views/index/header.php');
            include_once('views/universidad/confirm.php');
        }
    }

    function eliminarUniversidad() {
        if(isset($_POST['confirmacion']) && $_POST['confirmacion'] == 'confirmado') {
            if(isset($_POST['id_universidad'])) {
                $id_universidad = $_POST['id_universidad'];
                if ($id_universidad != 0) {
                    // Eliminar las filas relacionadas en universidades_carreras primero
                    $this->universidadModel->eliminarCarrerasUniversidad($id_universidad);
                    // Luego eliminar la universidad
                    $this->universidadModel->eliminarUniversidad($id_universidad);
                }
            }
        } else {
            // Manejar el caso en el que la confirmación no está establecida o no es 'confirmado'
        }
    }

}
?>
