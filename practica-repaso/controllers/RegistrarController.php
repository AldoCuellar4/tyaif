<?php 
        require_once('models/Registrar.php');

        class RegistrarController{

            private $registrarModel;

            function __construct(){
                $this->registrarModel=new RegistrarModelo();
            }

            function index(){
                include_once('views/index/header.php');
                include_once('views/registro/index.php');
                
            }
            

            //Función para hacer el registro
            function registro(){
                include_once('views/index/header.php');
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // obtener usuario y contraseña del formulario
                    $usuario = $_POST['nombre'];
                    $correo = $_POST['correo'];
                    $contraseña = $_POST['contraseña'];
            
                    //verificar los datos
                    $esUsuario = $this->registrarModel->insertarUsuario($usuario, $correo, $contraseña);
                    
                    //header("Location: index.php?metodo=login");

                } else {
                    include 'views/registro/index.php';
                }

            }


            //Función para poder cerrar sesión
            public function cerrarSesion(){
                // Cerrar la sesion y mandar al indexx
                session_start();
                session_destroy();
                header("Location: index.php?metodo=login");
                exit;
            }

        }
    ?>