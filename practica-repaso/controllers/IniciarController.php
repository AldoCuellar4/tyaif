<?php 
        require_once('models/Iniciar.php');

        class IniciarController{

            private $iniciarModel;

            function __construct(){
                $this->iniciarModel=new IniciarModelo();
            }

            function index(){
                include_once('views/index/header.php');
                include_once('views/iniciar/index.php');
                
            }
            
            function login(){
                include_once('views/index/header.php');
                include_once('views/iniciar/index.php');
                
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // obtener usuario y contraseña del formulario
                    $usuario = $_POST['nombre'];
                    $contraseña = $_POST['contraseña'];
            
                    // verificar los datos
                    $esUsuario = $this->iniciarModel->validarUsuario($usuario, $contraseña);
            
                    if ($esUsuario) {
                        session_start();
                        $_SESSION['esUsuario'] = true;
                        $_SESSION['username'] = $usuario;
                        header("Location: index.php?metodo=indexPrincipal");
                        exit;
                    } else {
                        header("Location: index.php?metodo=login");
                        exit;
                    }
                }
            }
            

            //Función para cerrar sesión
            public function cerrarSesion(){
                session_start();
                session_destroy();
                header("Location: index.php?metodo=login");
                exit;
            }

        }
    ?>