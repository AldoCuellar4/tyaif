<?php 
    require_once('models/Carrera.php');

    class CarreraController{

        private $carreraModel;

        function __construct(){
            $this->carreraModel=new CarreraModelo();
        }

        function index(){
            $query =$this->carreraModel->obtener();
            include_once('views/index/header.php');
            include_once('views/carrera/index.php');
        }
        
        // Obtener los datos de la carrera
        function carrera(){
            $data = array('id_carrera' => '');
            if(isset($_REQUEST['id_carrera'])){
                $data=$this->carreraModel->obtenerPorId($_REQUEST['id_carrera']);    
            }
            $query=$this->carreraModel->obtener();
            include_once('views/index/header.php');
            include_once('views/carrera/carrera.php');
        }
        
        function obtenerDatosEdicion(){ 
            $data['id_carrera'] = $_REQUEST['id_carrera'];
            $data['nombre_carrera'] = $_REQUEST['nombre_carrera'];
            $data['descripcion_carrera'] = $_REQUEST['descripcion_carrera'];
            if ($_REQUEST['id_carrera'] == "") {
                $this->carreraModel->crear($data);
            } else {
                $date = $_REQUEST['id_carrera'];
                $this->carreraModel->actualizar($data,$date);
            }
        }

        // Confirmar la eliminación de la carrera
        function confirmarEliminar() {
            if(isset($_REQUEST['id_carrera'])) {
                $id_carrera = $_REQUEST['id_carrera'];
                include_once('views/index/header.php');
                include_once('views/carrera/confirm.php');

            }
        }


        // Función para poder eliminar la carrera
        function eliminarCarrera() {
            if(isset($_POST['confirmacion']) && $_POST['confirmacion'] == 'confirmado') {
                if(isset($_POST['id_carrera'])) {
                    $id_carrera = $_POST['id_carrera'];
                    if ($id_carrera != 0) {
                        $this->carreraModel->eliminar($id_carrera);
                    }
                }
            }
        }
        
    }
?>
