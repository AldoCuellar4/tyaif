<?php
class Database
{
    private static $host = 'localhost' ;
    private static $user = 'admin';
    private static $pass = '1e2f315d7dee1fc36560ddb1f3c3f50db2ca295d1da255f1';
    private static $db = 'trabajorepaso' ;
     
    private static $cont  = null;
     
    public function __construct() {
        die('Init function is not allowed');
    }
     
    public static function connect()
    {
       if ( null == self::$cont )
       {     
        try
        {
          self::$cont =  new PDO( "mysql:host=".self::$host.";"."dbname=".self::$db, self::$user, self::$pass); 
        }
        catch(PDOException $e)
        {
          die($e->getMessage()); 
        }
       }
       return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
}
?>